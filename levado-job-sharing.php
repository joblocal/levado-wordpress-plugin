<?php
/*
 Plugin Name: Levado Job Sharing
 Plugin URI: http://joblocal.de
 Description: Share vacancies from joblocal job portals.
 Version: 1.0
 Author: Joblocal
 Author URI: http://joblocal.de/
 License: GPL2
*/

$joblocal_id = 0;

add_filter('widget_text', 'do_shortcode');

if ( ! function_exists('joblocal_vacancy'))
{
	function joblocal_vacancy($attibutes)
	{
		global $joblocal_id;

		$joblocal_id++;

		$defaults = array(
			'from' => get_option('levado_job_from'),
			'take' => get_option('levado_job_take'),
			'query' => get_option('levado_job_query'),
			'location' => get_option('levado_job_location'),
			'radius' => get_option('levado_job_radius'),
			'promoted' => get_option('levado_job_promoted'),
			'company' => get_option('levado_job_company'),
			'ad_type' => get_option('levado_ad_type')
		);

		$attributes = shortcode_atts($defaults, $attibutes);

		joblocal_get_template($attributes);
	}
}

if ( ! function_exists('joblocal_get_template'))
{
	function joblocal_get_template($attributes)
	{
		extract($attributes);

		$parameters = array('q' => $query, 'l' => $location, 'sr' => $radius, 'promoted' => $promoted, 'company_id' => $company, 'ad_type_id' => $ad_type);

		$response = joblocal_get_cache($parameters);

		if($response === false)
		{
			$data = joblocal_get_data($from, $parameters);

			if ( ! is_null($data) and isset($data['item']))
			{
				if ($take == '0')
				{
					$data['items'] = array();
				}
				else
				{
					if (empty($take))
					{
						$take = 5;
					}

					$data['items'] = array_slice($data['item'], 0, $take);
				}

				$defaults = [
					'link' => '',
					'image' => [
						'url' => '',
						'title' => ''
					],
					'items' => [
						[
							'id' => '',
							'link' => '',
							'title' => '',
							'company' => '',
							'address' => '',
							'description' => ''
						]
					]
				];

				$data = array_merge($defaults, $data);

				extract($data);

				ob_start();
					include(dirname(__FILE__).'/template.php');
					joblocal_set_cache(ob_get_contents(), $parameters);
				ob_end_flush();
			}
			else
			{
				// If nothing is return from the feed, we still want to cache an empty
				// string, to slow down requests.
				joblocal_set_cache('', $parameters);
			}
		}
		else
		{
			echo $response;
		}
	}
}

if ( ! function_exists('joblocal_get_data'))
{
	function joblocal_get_data($url, $parameters = array())
	{
		if ( ! empty($parameters))
		{
			$url .= '?';

			foreach ($parameters as $parameter => $value)
			{
				if ( ! empty($value)) $url .= $parameter . '=' . $value . '&';
			}

			$url = rtrim($url, '&');
		}

		$xml = @simplexml_load_file($url);

		if ($xml !== false)
		{
			$json = json_encode($xml);

			return json_decode($json, true);
		}

		return null;
	}
}

if ( ! function_exists('joblocal_set_cache'))
{
	function joblocal_set_cache($content, $parameters)
	{
		$hash = md5(implode(",", $parameters));

		$file = dirname(__FILE__) . '/cache/' . date('YmdH') . '-' . $hash . '.php';

		if ( ! file_exists($file))
		{
			joblocal_clear_cache();

			if ( ! is_dir(dirname(__FILE__) . '/cache/'))
			{
				mkdir(dirname(__FILE__) . '/cache/');
			}

			return @file_put_contents($file, $content);
		}
	}
}

if ( ! function_exists('joblocal_get_cache'))
{
	function joblocal_get_cache($parameters)
	{
		$hash = md5(implode(",", $parameters));

		$file = dirname(__FILE__) . '/cache/' . date('YmdH') . '-' . $hash . '.php';

		if (file_exists($file))
		{
			return @file_get_contents($file);
		}

		return false;
	}
}

if ( ! function_exists('joblocal_clear_cache'))
{
	function joblocal_clear_cache()
	{
		$files = glob(dirname(__FILE__) . '/cache/*');

		if (count($files) >= 1)
		{
			foreach($files as $file)
			{
				if(is_file($file) and strpos($file, date('YmdH') !== 0))
				{
					return @unlink($file);
				}
			}
		}
	}
}

add_shortcode('vacancy', 'joblocal_vacancy');

/*
|--------------------------------------------------------------------------
| Register Assets
|--------------------------------------------------------------------------
|
| Register all styles and scripts with high prority.
|
*/

if ( ! function_exists('joblocal_register_plugin_assets'))
{
	function joblocal_register_plugin_assets()
	{
		wp_register_style('levado-job-style', plugins_url('levado-job-sharing/css/plugin.css'));

		wp_enqueue_style('levado-job-style');
	}
}

add_action('wp_enqueue_scripts', 'joblocal_register_plugin_assets', 99);

/*
|--------------------------------------------------------------------------
| Custom Css
|--------------------------------------------------------------------------
*/

add_action('wp_head', 'joblocal_custom_css');

if ( ! function_exists('joblocal_custom_css'))
{
	function joblocal_custom_css()
	{
		echo '<style type="text/css">' . get_option('levado_job_css') . '</style>';
	}
}

/*
|--------------------------------------------------------------------------
| Options Page
|--------------------------------------------------------------------------
|
| Register the wordpress options page via add action and
| add_options_page. Then load and build the view.
|
*/

if (is_admin())
{
	add_action( 'admin_menu', 'levado_job_menu' );

	add_action( 'admin_init', 'levado_register_settings' );
}

if ( ! function_exists('levado_register_settings'))
{
	function levado_register_settings()
	{
		register_setting( 'levado-job-group', 'levado_job_from');

		register_setting( 'levado-job-group', 'levado_job_take');

		register_setting( 'levado-job-group', 'levado_job_query');

		register_setting( 'levado-job-group', 'levado_job_location');

		register_setting( 'levado-job-group', 'levado_job_radius');

		register_setting( 'levado-job-group', 'levado_job_promoted');

		register_setting( 'levado-job-group', 'levado_job_company');

		register_setting( 'levado-job-group', 'levado_ad_type');

		register_setting( 'levado-job-group', 'levado_job_alt');

		register_setting( 'levado-job-group', 'levado_job_show_logo');

		register_setting( 'levado-job-group', 'levado_job_css');
	}
}

if ( ! function_exists('levado_job_menu'))
{
	function levado_job_menu()
	{
		add_options_page('Levado Job Options', 'Levado Job Sharing', 'manage_options', 'levado-job-sharing', 'levado_job_options');
	}
}

if ( ! function_exists('levado_job_options'))
{
	function levado_job_options()
	{
		require(dirname(__FILE__).'/options.php');
	}
}

if ( ! function_exists('joblocal_get_url'))
{
	function getUrl()
	{
		$url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];

		$url .= $_SERVER["REQUEST_URI"];

		return $url;
	}
}
