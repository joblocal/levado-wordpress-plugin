<div class="levado-job-sharing">
	<?php if(get_option('levado_job_show_logo')) : ?>
		<a href="<?php echo $link ?>" class="levado-logo"><img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>"></a>
	<?php endif; ?>
	<?php if( ! isset($items) or empty($items)) : ?>
		<p><?php echo get_option('levado_job_alt'); ?></p>
	<?php else : ?>
		<ul class="levado-list">
			<?php if(isset($items['title'])) : ?>
				<li>
					<a href="<?php echo $item['link'] ?>?utm_source=<?php echo getUrl(); ?>&amp;utm_medium=referrer&amp;utm_content=anzeige<?php echo $item['id']; ?>&amp;utm_campaign=wordpress-plugin"><?php echo $item['title'] ?></a>
					<?php if( ! get_option('levado_job_company')) : ?>
						<p class="levado-company"><?php echo $item['company']; ?></p>
					<?php endif; ?>
					<p class="levado-address"><?php echo $item['address']; ?></p>
					<?php if( ! empty($item['description'])) : ?>
						<p class="levado-description"><?php echo $item['description']; ?></p>
					<?php endif; ?>
				</li>
			<?php else : ?>
				<?php foreach($items as $item) : ?>
					<li>
						<a href="<?php echo $item['link'] ?>?utm_source=<?php echo getUrl(); ?>&amp;utm_medium=referrer&amp;utm_content=anzeige<?php echo $item['id']; ?>&amp;utm_campaign=wordpress-plugin"><?php echo $item['title'] ?></a>
						<?php if( ! get_option('levado_job_company')) : ?>
							<p class="levado-company"><?php echo $item['company']; ?></p>
						<?php endif; ?>
						<p class="levado-address"><?php echo $item['address']; ?></p>
						<?php if( ! empty($item['description'])) : ?>
							<p class="levado-description"><?php echo $item['description']; ?></p>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	<?php endif; ?>
</div>